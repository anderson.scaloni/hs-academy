// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDhIVeE3CGk_dcNgdtR1J1fHZkjJznFmuw',
    authDomain: 'hs-academy-be7ea.firebaseapp.com',
    databaseURL: 'https://hs-academy-be7ea.firebaseio.com',
    projectId: 'hs-academy-be7ea',
    storageBucket: 'hs-academy-be7ea.appspot.com',
    messagingSenderId: '55406076588',
    appId: '1:55406076588:web:ba34cfad323780dbf9b509'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
